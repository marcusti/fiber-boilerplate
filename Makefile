include .env
export

BIN=./bin
GOPATH=$(shell go env GOPATH)
PACKAGES=./internal/...
PROJECT=gitlab.com/marcusti/fiber-boilerplate

build: init test-cover
	go build -o ${BIN}/fiber-boilerplate ./cmd/fiber-boilerplate

run:
	go run cmd/fiber-boilerplate/main.go

run-hotreload: ${GOPATH}/bin/reflex
	reflex -r '(\.go|.env|Makefile)' -s -- sh -c "go run cmd/fiber-boilerplate/main.go"

test:
	go test ${PACKAGES}

test-cover:
	go test -cover -coverprofile=coverage.txt -covermode atomic -race ${PACKAGES}
	go tool cover -func=coverage.txt

clean:
	rm -rf ${BIN}

tidy:
	go mod tidy

fmt:
	go fmt ./...

lint: ${GOPATH}/bin/golint
	golint ./...

vet:
	go vet ./...

init: tidy fmt lint vet
	mkdir -p ${BIN}

update:
	go get -u ./...

${GOPATH}/bin/golint:
	go get -u golang.org/x/lint/golint

${GOPATH}/bin/mockgen:
	go install github.com/golang/mock/mockgen@latest

${GOPATH}/bin/reflex:
	go install github.com/cespare/reflex@latest

.PHONY: build clean fmt init lint run run-hotreload test test-cover tidy update vet
