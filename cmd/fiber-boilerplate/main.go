package main

import (
	"log"

	"github.com/joho/godotenv"

	"gitlab.com/marcusti/fiber-boilerplate/internal/app"
	"gitlab.com/marcusti/fiber-boilerplate/internal/app/config"
)

func main() {
	// load environment variables from .env file
	err := godotenv.Load(".env")
	if err != nil {
		log.Println(err)
	}

	// create application
	cfg := config.DefaultConfig()
	app, err := app.New(cfg)
	if err != nil {
		log.Fatal(err)
	}

	// start the HTTP server
	if err := app.Start(); err != nil {
		log.Fatal(err)
	}
}
