package app

import "github.com/gofiber/fiber/v2"

func (a *App) createAPIRoutes() {
	api := a.Fiber.Group("/api")
	api.Get("/ping", a.ping)

}

func (a *App) ping(c *fiber.Ctx) error {
	return c.SendString("OK")
}
