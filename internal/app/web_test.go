package app_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIndex(t *testing.T) {
	a := createTestApp(t)
	req := httptest.NewRequest("GET", "/", nil)
	res, err := a.Fiber.Test(req)
	assert.NoError(t, err)
	assert.Equal(t, res.StatusCode, http.StatusOK)
	assert.Equal(t, res.Header.Get(contentType), textHTML)
}
