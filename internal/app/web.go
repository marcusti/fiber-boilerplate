package app

import "github.com/gofiber/fiber/v2"

func (a *App) createWebRoutes() {
	api := a.Fiber.Group("/")
	api.Get("/", a.index)

}

func (a *App) index(c *fiber.Ctx) error {
	return c.Render("index", fiber.Map{})
}
