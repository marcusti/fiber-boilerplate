package app

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/favicon"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/gofiber/template/django"

	"gitlab.com/marcusti/fiber-boilerplate/internal/app/config"
)

type (
	// App holds application state.
	App struct {
		Config     config.Config
		Fiber      *fiber.App
		interrupts chan os.Signal
	}
)

// New creates a new application instance.
func New(cfg config.Config) (*App, error) {
	// create a channel for OS interrupt events
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	// create a view template engine instance
	templateEngine := django.New(cfg.TemplatesDir, cfg.TemplatesExtension)
	templateEngine.Reload(cfg.TemplatesReload)

	// create an application instance
	a := App{
		Config: cfg,
		Fiber: fiber.New(fiber.Config{
			IdleTimeout:  cfg.Timeout,
			ReadTimeout:  cfg.Timeout,
			WriteTimeout: cfg.Timeout,

			Prefork: cfg.Prefork,
			Views:   templateEngine,
		}),
		interrupts: c,
	}

	// setup default middlewares
	a.Fiber.Use(recover.New())
	a.Fiber.Use(requestid.New())
	a.Fiber.Use(logger.New(logger.Config{
		Format:     cfg.LoggingFormat,
		TimeFormat: cfg.LoggingTimeFormat,
		TimeZone:   cfg.LoggingTimeZone,
	}))
	a.Fiber.Use(favicon.New())

	// create routes
	a.createAPIRoutes()
	a.createWebRoutes()

	return &a, nil
}

// Start starts the HTTP server.
func (a *App) Start() error {
	// create a background task that waits for OS interrupt signals
	go func() {
		<-a.interrupts
		if !fiber.IsChild() { // prevent multiple logging in PREFORK mode
			log.Println("Gracefully shutting down")
		}
		if err := a.Fiber.Shutdown(); err != nil {
			log.Panic(err)
		}
	}()

	// start the server
	return a.Fiber.Listen(fmt.Sprintf("%s:%d", a.Config.Host, a.Config.Port))
}

// Shutdown gracefully shuts down the HTTP server.
func (a *App) Shutdown() {
	a.interrupts <- os.Interrupt
}
