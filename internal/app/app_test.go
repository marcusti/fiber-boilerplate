package app_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/marcusti/fiber-boilerplate/internal/app"
	"gitlab.com/marcusti/fiber-boilerplate/internal/app/config"
)

const (
	contentType = "Content-Type"
	textHTML    = "text/html; charset=utf-8"
	textPlain   = "text/plain; charset=utf-8"
)

var (
	host    = "127.0.0.1"
	port    = uint16(9999)
	baseURL = fmt.Sprintf("http://%s:%d", host, port)
)

func createTestConfig() config.Config {
	cfg := config.DefaultConfig()
	cfg.Host = host
	cfg.Port = port
	cfg.TemplatesDir = "../../templates"
	cfg.Timeout = time.Second
	return cfg
}

func createTestApp(t *testing.T) *app.App {
	a, err := app.New(createTestConfig())
	assert.NoError(t, err)
	return a
}

func TestApplicationStartAndShutdown(t *testing.T) {
	cfg := createTestConfig()

	a, err := app.New(cfg)
	assert.NoError(t, err)

	go func() {
		err = a.Start()
		assert.NoError(t, err)
	}()

	time.Sleep(100 * time.Millisecond)

	res, err := http.DefaultClient.Get(baseURL + "/")
	assert.NoError(t, err)
	assert.Equal(t, 200, res.StatusCode)
	assert.Contains(t, res.Header[contentType], textHTML)

	res, err = http.DefaultClient.Get(baseURL + "/api/ping")
	assert.NoError(t, err)
	assert.Equal(t, 200, res.StatusCode)
	assert.Contains(t, res.Header[contentType], textPlain)
	bytes, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, "OK", string(bytes))

	a.Shutdown()
}
