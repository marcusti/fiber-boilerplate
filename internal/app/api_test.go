package app_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPing(t *testing.T) {
	a := createTestApp(t)
	req := httptest.NewRequest("GET", "/api/ping", nil)
	res, err := a.Fiber.Test(req)
	assert.NoError(t, err)
	assert.Equal(t, res.StatusCode, http.StatusOK)
	assert.Equal(t, res.Header.Get(contentType), textPlain)
	bytes, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, "OK", string(bytes))
}
