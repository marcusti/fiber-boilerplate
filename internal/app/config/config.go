package config

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
)

const (
	// Masked indicates that an environment variable value should be logged masked
	Masked Mask = iota

	// PlainText indicates that an environment variable value should be logged as plain text
	PlainText
)

type (
	// Mask is used as an enum type
	Mask int

	// Config configuration data
	Config struct {
		DbURL   string
		Host    string
		Port    uint16
		Prefork bool
		Timeout time.Duration

		LoggingFormat     string
		LoggingTimeFormat string
		LoggingTimeZone   string

		TemplatesDir       string
		TemplatesExtension string
		TemplatesReload    bool
	}
)

// DefaultConfig creates a default configuration instance.
func DefaultConfig() Config {
	return Config{
		DbURL:   getEnvOrDefault("DATABASE_URL", "postgresql://127.0.0.1/db", Masked),
		Host:    getEnvOrDefault("HOST", "127.0.0.1", PlainText),
		Port:    getEnvAsUint16("PORT", 3000, PlainText),
		Prefork: getEnvAsBool("PREFORK", false, PlainText),

		LoggingFormat:     "${time} ${pid} ${ips} ${locals:requestid} ${status} - ${method} ${path}​ ${latency}\n​",
		LoggingTimeFormat: "2006-01-02T15:04:05.999Z07:00",
		LoggingTimeZone:   "Local",

		TemplatesDir:       "./templates",
		TemplatesExtension: ".html",
		TemplatesReload:    true,
		Timeout:            20 * time.Second,
	}
}

// getEnvOrDefault returns an environment variable, or a default value.
// Logs the environment variable name and value, which can be controlled with the masked flag.
func getEnvOrDefault(key, defaultValue string, mask Mask) string {
	val := strings.TrimSpace(os.Getenv(key))
	if val == "" {
		val = defaultValue
	}

	if !fiber.IsChild() { // prevent multiple logging in PREFORK mode
		display := val
		if mask == Masked {
			display = "***"
		}
		log.Printf("%s=%s", key, display)
	}

	return val
}

// getEnvAsUint16 returns an environment variable as an uint16, or a default value.
func getEnvAsUint16(key string, defaultValue uint16, mask Mask) uint16 {
	s := getEnvOrDefault(key, fmt.Sprint(defaultValue), mask)
	// error can be ignored here because defaultValue is already the correct type
	val, _ := strconv.ParseUint(s, 10, 16)
	return uint16(val)
}

// getEnvAsBool returns an environment variable as bool, or a default value.
func getEnvAsBool(key string, defaultValue bool, mask Mask) bool {
	s := getEnvOrDefault(key, fmt.Sprint(defaultValue), mask)
	// error can be ignored here because defaultValue is already the correct type
	val, _ := strconv.ParseBool(s)
	return val
}
