package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

const key = "FIBER_BOILERPLATE_TEST"

func TestDefaultConfig(t *testing.T) {
	cfg := DefaultConfig()
	assert.Equal(t, "./templates", cfg.TemplatesDir)
}

func TestGetEnvAndExpectEnvVarValue(t *testing.T) {
	os.Setenv(key, "bar")
	assert.Equal(t, getEnvOrDefault(key, "xxx", PlainText), "bar")
	os.Unsetenv(key)
}

func TestGetEnvAndExpectDefaultValue(t *testing.T) {
	os.Unsetenv(key)
	assert.Equal(t, getEnvOrDefault(key, "xxx", PlainText), "xxx")
}

func TestGetEnvAsBoolAndExpectEnvVarValue(t *testing.T) {
	os.Setenv(key, "true")
	assert.True(t, getEnvAsBool(key, false, PlainText))
	os.Unsetenv(key)
}

func TestGetEnvAsBoolAndExpectDefaultValue(t *testing.T) {
	os.Unsetenv(key)
	assert.False(t, getEnvAsBool(key, false, PlainText))
}

func TestGetEnvAsUint16AndExpectEnvVarValue(t *testing.T) {
	os.Setenv(key, "4711")
	assert.Equal(t, getEnvAsUint16(key, 42, PlainText), uint16(4711))
	os.Unsetenv(key)
}

func TestGetEnvAsUint16AndExpectDefaultValue(t *testing.T) {
	os.Unsetenv(key)
	assert.Equal(t, getEnvAsUint16(key, 42, PlainText), uint16(42))
}
