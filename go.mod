module gitlab.com/marcusti/fiber-boilerplate

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/gofiber/template v1.6.16
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.13.5 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/valyala/fasthttp v1.30.0 // indirect
	golang.org/x/sys v0.0.0-20210910150752-751e447fb3d0 // indirect
)
